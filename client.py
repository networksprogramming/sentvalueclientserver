import socket

# กำหนดข้อมูลของเซิร์ฟเวอร์
HOST = 'angsila.informatics.buu.ac.th'
PORT = 12345  # แทนที่ด้วยพอร์ตที่ถูกต้อง

# สร้าง socket object
client_socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)

try:
    # เชื่อมต่อกับเซิร์ฟเวอร์
    client_socket.connect((HOST, PORT))
    print('เชื่อมต่อสำเร็จ')

    # รับขนาดของแฟ้ม
    file_size_str = client_socket.recv(1024).decode()
    file_size = int(file_size_str)

    # รับข้อมูลแฟ้ม
    received_data = b''
    while len(received_data) < file_size:
        data = client_socket.recv(1024)
        received_data += data

    # บันทึกข้อมูลเป็นแฟ้ม
    file_name = 'received_file.txt'
    with open(file_name, 'wb') as file:
        file.write(received_data)

    print('รับแฟ้มเรียบร้อย')

    # แสดงผลที่อ่านมาจากแฟ้ม
    with open(file_name, 'r') as file:
        file_content = file.read()
        print('ข้อมูลในแฟ้ม:')
        print(file_content)
except Exception as e:
    print('ไม่สามารถเชื่อมต่อหรือรับแฟ้มได้:', str(e))

# ปิดการเชื่อมต่อ
client_socket.close()