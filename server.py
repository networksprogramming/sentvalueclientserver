import socket

# กำหนดข้อมูลของเซิร์ฟเวอร์
HOST = 'angsila.informatics.buu.ac.th'
PORT = 12345  # แทนที่ด้วยพอร์ตที่ถูกต้อง

# สร้าง socket object
server_socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)

# ผูกเซิร์ฟเวอร์กับที่อยู่และพอร์ต
server_socket.bind((HOST, PORT))

# รอการเชื่อมต่อ
server_socket.listen()

print('รอการเชื่อมต่อจาก Client...')

# รับการเชื่อมต่อจาก Client
client_socket, client_address = server_socket.accept()
print('เชื่อมต่อกับ:', client_address)

# อ่านแฟ้ม
file_name = 'a.txt'
with open(file_name, 'rb') as file:
    file_data = file.read()

# ส่งขนาดของแฟ้ม
file_size = len(file_data)
client_socket.send(str(file_size).encode())

# ส่งข้อมูลแฟ้ม
client_socket.sendall(file_data)

print('ส่งแฟ้มเรียบร้อย')

# ปิดการเชื่อมต่อ
client_socket.close()
server_socket.close()